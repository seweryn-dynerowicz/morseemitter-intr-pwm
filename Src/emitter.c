#include "morse.h"
#include "emitter.h"

typedef enum { LO = 0, HI = 1 } State;

TIM_HandleTypeDef pwm_source;

char* messageToSend = NULL;
uint8_t currentSymbol = 0;

State currentPulseState = LO;
char* pulses = NULL;
uint8_t currentPulse = 0;

void startPulsing() { HAL_TIM_PWM_Start(&pwm_source, TIM_CHANNEL_2); }
void ceasePulsing() { HAL_TIM_PWM_Stop (&pwm_source, TIM_CHANNEL_2); }

uint8_t emitter_transition() {
	uint8_t duration = 1;
	switch(currentPulseState) {
	// LO -> HI : set duration according to the current pulse
	case LO :
		switch(pulses[currentPulse]) {
			case '.' : duration = DIT; break;
			case '-' : duration = DAT; break;
		}
		startPulsing();
		currentPulseState = HI;
		break;
	// HI -> LO : advance to the next pulse
	case HI:
		currentPulse += 1;
		if(pulses[currentPulse] == '\0') {
			currentSymbol += 1;
			currentPulse = 0;
			switch(messageToSend[currentSymbol]) {
				case '\0': duration = 14; currentSymbol = 0; break;
				case ' ' : duration = 7; currentSymbol += 1; break;
			default : duration = 3; break;
			}
			pulses = getPulses(messageToSend, currentSymbol);
		} else
			duration = 1;
		ceasePulsing();
		currentPulseState = LO;
		break;
	}
	return duration;
}

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Initialization of emitter */
void initialize_emitter(char* message) {
	messageToSend = message;
	pulses = getPulses(message, currentSymbol);

	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_OC_InitTypeDef sConfigOC;

	pwm_source.Instance = TIM2;
	pwm_source.Init.Prescaler = PWM_PRESCALER;
	pwm_source.Init.CounterMode = TIM_COUNTERMODE_UP;
	pwm_source.Init.Period = PWM_PERIOD;
	pwm_source.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	pwm_source.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&pwm_source) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&pwm_source, &sClockSourceConfig) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	if (HAL_TIM_PWM_Init(&pwm_source) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = PWM_PULSE_WIDTH;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&pwm_source, &sConfigOC, TIM_CHANNEL_2) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

  	HAL_TIM_MspPostInit(&pwm_source);
}
