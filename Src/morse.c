#include "morse.h"

char* letters[26] = {
	".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....",
	"..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-",
	".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."
};

char* numbers[10] = {
	"-----", ".----", "..---", "...--", "....-",
	".....", "-....", "--...", "---..", "----."
};

char* getPulses(char* message, uint8_t symbolIndex) {
	char* pulses = NULL;
	char symbol = message[symbolIndex];
	if('a' <= symbol && symbol <= 'z')
		pulses = letters[symbol - 'a'];
	else if ('0' <= symbol && symbol <= '9')
		pulses = numbers[symbol - '0'];
	return pulses;
}
