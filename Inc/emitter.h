#ifndef EMITTER_H_
#define EMITTER_H_

#include "stm32f3xx_hal.h"

#define PWM_PULSE_WIDTH  4
#define PWM_PERIOD      20
#define PWM_FREQUENCY 1000
// Formula from https://letanphuc.net/2015/06/stm32f0-timer-tutorial-and-counter-tutorial
#define PWM_PRESCALER ((HSI_VALUE / (PWM_PERIOD * PWM_FREQUENCY)) - 1)

void initialize_emitter(char* message);
uint8_t emitter_transition();

#endif /* EMITTER_H_ */
