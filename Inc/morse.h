#ifndef MORSE_H_
#define MORSE_H_

#include "stm32f3xx_hal.h"

#define DIT 1 // '.'
#define DAT 3 // '-'

extern char* letters[];
extern char* numbers[];

char* getPulses(char* message, uint8_t symbolIndex);

#endif /* MORSE_H_ */
